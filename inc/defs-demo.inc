<?php

	/*
	 * 
	 * Constants only for demo page (demo.php) - DON'T EDIT IF YOU DO NOT KNOW EXACTLY WHAT DO YOU DO
	 * 
	 */
	define ('D_VERSION', '1.0.15');										// Version of demo script
	define ('D_ID_CHANGE_ITEM', '2');									// ID for change some item (output, marker, ...)
	define ('D_ID_CHANGE_DATETIME', '3');								// ID for change date and time in LOGO!
	define ('D_BYTE', '1');												// Number of bytes for byte
	define ('D_WORD', '2');												// Number of bytes fo word
	define ('D_DWORD', '4');											// Number of bytes for double word
	$GLOBALS ['bytes'] = array (
								0 => array ('name' => 'byte', 'lenght' => '1'),
								1 => array ('name' => 'word', 'lenght' => '2'),
								2 => array ('name' => 'dword', 'lenght' => '4')
								);
	$GLOBALS ['month'] = array (1 => 'january', 2 => 'february', 3 => 'march', 4 => 'april', 5 => 'may', 6 => 'june', 7 => 'july', 8 => 'august', 9 => 'september', 10 => 'october', 11 => 'november', 12 => 'december');	// Array of months in english
	$GLOBALS ['monthcz'] = array (1 => 'ledna', 2 => 'února', 3 => 'března', 4 => 'dubna', 5 => 'května', 6 => 'června', 7 => 'července', 8 => 'srpna', 9 => 'září', 10 => 'října', 11 => 'listopadu', 12 => 'prosince');	// Array of months in czech
	$GLOBALS ['day'] = array (1 => 'monday', 2 => 'tuesday', 3 => 'wednesday', 4 => 'thursday', 5 => 'friday', 6 => 'saturday', 7 => 'sunday');	// Array of days in english
	$GLOBALS ['daycz'] = array (1 => 'pondělí', 2 => 'úterý', 3 => 'středa', 4 => 'čtvrtek', 5 => 'pátek', 6 => 'sobota', 7 => 'neděle');		// Array of days in czech

?>
