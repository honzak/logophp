<?php

	/*
	 * 
	 * List of functions:
	 * 		f_get_msg_connect ($t_tsap_src, $t_tsap_dest)
	 * 		f_get_msg_setup ()
	 * 		f_connect_logos ()
	 * 		f_disconnect_logos ($a_socket)
	 * 		f_read_datetime ($s_socket)
	 * 		f_write_datetime ($s_socket, $a_datetime)
	 * 		f_get_msg_read_bytes ($t_area, $i_addr, $i_count)
	 * 		f_read_bits ($s_socket, $t_area, $i_items)
	 * 		f_read_bits2 ($s_socket, $t_area, $i_items)
	 * 		f_read_bytes ($s_socket, $t_area, $i_items)
	 * 		f_write_bit ($s_socket, $t_area, $i_addr_byte, $i_addr_bit, $i_value)
	 * 		f_write_bit2 ($s_socket, $t_area, $i_addr_byte, $i_addr_bit, $i_value)
	 * 		f_write_bytes ($s_socket, $i_addr, $a_vars)
	 * 		f_get_value ($i_addr, $i_bytes, $a_vars)
	 * 		f_set_value ($s_socket, $i_addr, $i_bytes, $i_value)
	 * 
	 * About communication protocol, I don't understand exactly to everything, mostly copy communication read by Wireshark... but it works ;-)
	 * It all comes out from RFC 905 about network protocol:
	 * 		4. layer - TPKT
	 * 		5. layer - ISO 8073/X.224 COTP Connection-Oriented Transport Protocol
	 * 		6. layer - S7 Communication
	 * 
	 */



	/*
	 * 
	 * Make string for create connection with LOGO!
	 * 
	 * 		Input:			Nothing
	 * 
	 * 		Output:			String of hex chars
	 * 
	 * 		Note:			SRC Reference is expecting as value of SESSION ($_SESSION ['src_reference'])
	 *
	 * 		Wireshark:
	 *			Protocol:	COTP
	 * 			Line:		CR - Connection Request
	 * 			Answer:		CC - Connection Confirm.
	 * 
	 */
	function f_get_msg_connect ($t_tsap_src, $t_tsap_dest)
	{

		$a_tsap_src = explode (".", $t_tsap_src);
		$a_tsap_dest = explode (".", $t_tsap_dest);

		// ISO-COTP
		$t_msg_iso_cotp = D_PDU_TYPE_CR;											// PDU Type
		$t_msg_iso_cotp .= "\x00\x00";												// ? - Destination reference
		$t_msg_iso_cotp .= pack ("n", $_SESSION ['src_reference']++);				// Source reference
		$t_msg_iso_cotp .= D_CLASS_00;												// Class
		$t_msg_iso_cotp .= D_PAR_CODE_TPDU_SIZE;									// Parameter code
		$t_msg_iso_cotp .= "\x01";													// Parameter lenght
		$t_msg_iso_cotp .= "\x09";													// ? - TPDU size
		$t_msg_iso_cotp .= D_PAR_CODE_SRC_TSAP;										// Parameter code
		$t_msg_iso_cotp .= "\x02";													// Parameter lenght
		$t_msg_iso_cotp .= pack ("n", hexdec ($a_tsap_src [0] . $a_tsap_src [1]));	// Source TSAP
		$t_msg_iso_cotp .= D_PAR_CODE_DST_TSAP;										// Parameter code
		$t_msg_iso_cotp .= "\x02";													// Parameter lenght
		$t_msg_iso_cotp .= pack ("n", hexdec ($a_tsap_dest [0] . $a_tsap_dest [1]));// Destination TSAP
		$t_msg_iso_cotp = pack ("C", strlen ($t_msg_iso_cotp)) . $t_msg_iso_cotp;	// Lenght (1 Byte)

		// TPKT
		$t_msg_tpkt = D_TPKT_VERSION;												// Version
		$t_msg_tpkt .= D_RES;														// Reserved
		$t_msg_tpkt .= pack ("n", (strlen ($t_msg_iso_cotp) + 4));					// Lenght

		return $t_msg_tpkt . $t_msg_iso_cotp;

	} // f_get_msg_connect



	/*
	 * 
	 * Make string for setup connection with LOGO!
	 * 
	 * 		Input:		Nothing
	 * 
	 * 		Output:		String of hex chars
	 * 
	 *		Note:		PDU Reference is expecting as value of SESSION ($_SESSION ['pdu_reference']).
	 * 
	 *		Wireshark:
	 * 			Protocol:	S7COMM
	 * 			Line:		ROSCTR [Job] Function [Setup Communication]
	 * 			Answer:		ROSCTR [Ack_Data] Function [Setup Communication].
	 * 
	 */
	function f_get_msg_setup ()
	{

		// S7
		$t_msg_s7_data = "";
		$t_msg_s7_param = D_F_SETUP_COMM;											// Function
		$t_msg_s7_param .= D_RES;													// Reserved
		$t_msg_s7_param .= "\x00\x01";												// ? - Max AmQ (parallel jobs with ack) calling
		$t_msg_s7_param .= "\x00\x01";												// ? - Max AmQ (parallel jobs with ack) called
		$t_msg_s7_param .= "\x01\xe0";												// ? - PDU lenght
		$t_msg_s7_header = D_PROTOCOL_ID;											// Proticol Id
		$t_msg_s7_header .= D_ROSCTR_JOB;											// ROSCTR
		$t_msg_s7_header .= D_RES . D_RES;											// Redundancy Idetification (reserved)
		$t_msg_s7_header .= pack ("n", $_SESSION ['pdu_reference']++);				// Protocol Data Unit Reference
		$t_msg_s7_header .= pack ("n", strlen ($t_msg_s7_param));					// Parameter lenght (2 Bytes)
		$t_msg_s7_header .= pack ("n", strlen ($t_msg_s7_data));					// Data lenght (2 bytes)
		$t_msg_s7 = $t_msg_s7_header . $t_msg_s7_param . $t_msg_s7_data;

		// ISO-COTP
		$t_msg_iso_cotp = D_PDU_TYPE_DT;											// PDU type
		$t_msg_iso_cotp .= "\x80";													// ???
		$t_msg_iso_cotp = pack ("C", strlen ($t_msg_iso_cotp)) . $t_msg_iso_cotp;	// Lenght

		// TPKT
		$t_msg_tpkt = D_TPKT_VERSION;												// Version
		$t_msg_tpkt .= D_RES;														// Reserved
		$t_msg_tpkt .= pack ("n", (strlen ($t_msg_iso_cotp . $t_msg_s7) + 4));		// Lenght

		return $t_msg_tpkt . $t_msg_iso_cotp . $t_msg_s7;

	} // f_get_msg_setup



	/*
	 * 
	 * Function for creating sockets to all of LOGOS from variable $GLOBALS ["logos"] and setup communication
	 * 
	 * 		Input:		Nothing
	 * 
	 * 		Output:		Array of sockets or FALSE if fail
	 * 
	 */
	function f_connect_logos ()
	{
		$a_socket = array (); 
		$i_logo_num = 0;
		foreach ($GLOBALS ["logos"] as $a_logo)
		{
			$a_socket [$i_logo_num] = socket_create (AF_INET, SOCK_STREAM, SOL_TCP);					// Create TCP/IP socket
			if ($a_socket [$i_logo_num] !== false)														// Socket OK?
			{
				$b_result = socket_connect ($a_socket [$i_logo_num], $a_logo ['ip'], D_LOGO_PORT);		// Connection to IP and port
				if ($b_result === true)																	// Connection OK?
				{
					$t_msg_connect = f_get_msg_connect ($a_logo ['tsap-src'], $a_logo ['tsap-dest']);	// Make string for CR (Connect Request)
					socket_write ($a_socket [$i_logo_num], $t_msg_connect, strlen ($t_msg_connect));	// Sending CR
					socket_read ($a_socket [$i_logo_num], 76);											// Read answer, but I don't care about result. It is mainly waiting for reply.
					$t_msg_setup = f_get_msg_setup ();													// Make string for setup communication with LOGO! (Setup Communication)
					socket_write ($a_socket [$i_logo_num], $t_msg_setup, strlen ($t_msg_setup));		// Sending Setup Communication
					socket_read ($a_socket [$i_logo_num], 81);											// Read answer, but I don't care about result. It is mainly waiting for reply.
				} // if
				else
					$a_socket [$i_logo_num] = false;
			} // if
			$i_logo_num++;
		} // foreach
		return $a_socket;
	} // f_connect_logo



	/*
	 * 
	 * Close socket all LOGO!s from variable $GLOBALS ["logos"]
	 * 
	 * 		Input:		Array of sockets
	 * 
	 * 		Output:		Nothing
	 * 
	 * 		Notes:		Before closing socket I send request for readin input I1, but I don't read answer and immediately then close socket.
	 * 					It is workaround how close connection with LOGO! almost immediately.
	 * 					Otherwise PHP close socket but I mean that LOGO! wait for any request yet and close this connection after 10s of inactivity or timeout.
	 * 					LOGO! keep up 10 connections at the same time, so it is possible that LOGO! wouldn't reply without this workaround.
	 * 
	 */
	function f_disconnect_logos ($a_socket)
	{
		foreach ($a_socket as $s_socket)
		{
			if ($s_socket !== false)										// Socket OK?
			{
				$t_msg = f_get_msg_read_bytes ('inputs', 0, 1);				// String for reading input I1
				socket_write ($s_socket, $t_msg, strlen ($t_msg));			// Sending read I1
				socket_close ($s_socket);									// Close socket
			} // if
		} // foreach
	} // f_disconnect_logos



	/*
	 * 
	 * Reading date and time of LOGO!
	 * 
	 * 		Input:		Socket
	 * 
	 * 		Output:		Array of six attributes: 'year', 'month', 'day', 'hour', 'minute' a 'second'.
	 * 
	 * 		Notes:		PDU Reference is expecting as value of SESSION ($_SESSION ['pdu_reference']).
	 * 					Possible only for version 8 (0BA8), not works on version 7 (0BA7).
	 *
	 * 		Example:	$a_date ['year']
	 */
	function f_read_datetime ($s_socket)
	{

		// S7 protocol
		$t_msg_s7_param = D_F_READ_VAR;												// Function
		$t_msg_s7_param .= "\x01";													// Item count
		$t_msg_s7_param .= "\x12";													// ? - Variable specification: 0x12
		$t_msg_s7_param .= "\x0a";													// ? - Lenght of following address specification: 10
		$t_msg_s7_param .= D_SYNTAX_ID_S7ANY;										// ? - Syntax Id
		$t_msg_s7_param .= D_TS_BYTE_PAR;											// Transport size
		$t_msg_s7_param .= "\x00\x06";												// Length: 6 znaku
		$t_msg_s7_param .= "\x00\x01";												// DB number: 0 = DB0
		$t_msg_s7_param .= "\x84";													// Area: 0x84 = DB
		$t_msg_s7_param .= "\x00" . pack ("n", (985 << 3));							// Address
		$t_msg_s7_header = D_PROTOCOL_ID;											// Proticol Id
		$t_msg_s7_header .= D_ROSCTR_JOB;											// ROSCTR
		$t_msg_s7_header .= D_RES . D_RES;											// Redundancy Idetification (reserved)
		$t_msg_s7_header .= pack ("n", $_SESSION ['pdu_reference']++);				// Protocol Data Unit Reference
		$t_msg_s7_header .= pack ("n", strlen ($t_msg_s7_param));					// Parameter lenght (2 Bytes)
		$t_msg_s7_header .= "\x00\x00";												// Data lenght (2 bytes)
		$t_msg_s7 = $t_msg_s7_header . $t_msg_s7_param;

		// ISO-COTP
		$t_msg_iso_cotp = D_PDU_TYPE_DT;											// PDU type
		$t_msg_iso_cotp .= "\x80";													// ???
		$t_msg_iso_cotp = pack ("C", strlen ($t_msg_iso_cotp)) . $t_msg_iso_cotp;	// Lenght

		// TPKT
		$t_msg_tpkt = D_TPKT_VERSION;												// Version
		$t_msg_tpkt .= D_RES;														// Reserved
		$t_msg_tpkt .= pack ("n", (strlen ($t_msg_iso_cotp . $t_msg_s7) + 4));		// Lenght

		$t_msg_read = $t_msg_tpkt . $t_msg_iso_cotp . $t_msg_s7;

		socket_write ($s_socket, $t_msg_read, strlen ($t_msg_read));
		$t_return = socket_read ($s_socket, 31);									// $t_return = "\x03\x00\x00\x24\x02\x00\x80\x.....
		$a_return = unpack ("C" . strlen ($t_return), $t_return);					// Convert to chars (1-byte integer) and store in array
		$a_date = array ('year' => $a_return [26], 'month' => $a_return [27], 'day' => $a_return [28], 'hour' => $a_return [29], 'minute' => $a_return [30], 'second' => $a_return [31]);

		return ($a_date);

	} // f_read_datetime



	/*
	 * Save date and time to LOGO!
	 * 
	 * 		Vstup:		Socket
	 * 					Array of six attributes: 'year', 'month', 'day', 'hour', 'minute' a 'second'.
	 * 
	 * 		Vystup:		Nothing
	 * 
	 * 		Notes:		PDU Reference is expecting as value of SESSION ($_SESSION ['pdu_reference']).
	 * 					Possible only for version 8 (0BA8), not works on version 7 (0BA7).
	 * 
	 */
	function f_write_datetime ($s_socket, $a_datetime)
	{

		// S7 protocol
		$t_msg_s7_data = D_RES;														// Return code: Reserved
		$t_msg_s7_data .= D_TS_BYTE_DATA;											// Transport size
		$t_msg_s7_data .= "\x00\x30";												// Lenght: 6 bajtu => 48 bits => 0x30
		$t_msg_s7_data .= pack ("C6", $a_datetime ['year'], $a_datetime ['month'], $a_datetime ['day'], $a_datetime ['hour'], $a_datetime ['minute'], $a_datetime ['second']);	// Data
		$t_msg_s7_param = D_F_WRITE_VAR;											// Function
		$t_msg_s7_param .= "\x01";													// Item count
		$t_msg_s7_param .= "\x12";													// ? - Variable specification
		$t_msg_s7_param .= "\x0a";													// ? - Lenght of following address specification
		$t_msg_s7_param .= D_SYNTAX_ID_S7ANY;										// ? - Syntax Id
		$t_msg_s7_param .= D_TS_BYTE_PAR;											// Transport size
		$t_msg_s7_param .= "\x00\x06";												// Lenght: 6 bajtu
		$t_msg_s7_param .= "\x00\x00";												// DB number
		$t_msg_s7_param .= D_AREA_DB;												// Area
		$t_msg_s7_param .= "\x00" . pack ("n", (985 << 3));							// Address
		$t_msg_s7_header = D_PROTOCOL_ID;											// Protocol Id
		$t_msg_s7_header .= D_ROSCTR_JOB;											// ROSCTR
		$t_msg_s7_header .= D_RES . D_RES;											// Redundancy Identification (Reserved)
		$t_msg_s7_header .= pack ("n", $_SESSION ['pdu_reference']++);				// Protocol Data Unit Reference
		$t_msg_s7_header .= pack ("n", strlen ($t_msg_s7_param));					// Parameter lenght (2 Bytes)
		$t_msg_s7_header .= pack ("n", strlen ($t_msg_s7_data));					// Data lenght (2 bytes)
		$t_msg_s7 = $t_msg_s7_header . $t_msg_s7_param . $t_msg_s7_data;
	
		// ISO-COTP
		$t_msg_iso_cotp = D_PDU_TYPE_DT;											// PDU type
		$t_msg_iso_cotp .= "\x80";													// ???
		$t_msg_iso_cotp = pack ("C", strlen ($t_msg_iso_cotp)) . $t_msg_iso_cotp;	// Lenght

		// TPKT
		$t_msg_tpkt = D_TPKT_VERSION;												// Version
		$t_msg_tpkt .= D_RES;														// Reserved
		$t_msg_tpkt .= pack ("n", (strlen ($t_msg_iso_cotp . $t_msg_s7) + 4));		// Lenght

		$t_msg_write = $t_msg_tpkt . $t_msg_iso_cotp . $t_msg_s7;
		socket_write ($s_socket, $t_msg_write, strlen ($t_msg_write));
		socket_read ($s_socket, 22);

		return 1;

	} // f_write_datetime



	/*
	 * 
	 * Make string for reading one/more byte/s from area inputs/outputs/markers/VM.
	 * 
	 * 		Input:		Area (possibilities: $_GLOBALS ['area'])
	 * 					Address of first reading byte (0 - ?)
	 * 					Number of bytes for reading (length)
	 * 
	 * 		Output:		String of hex chars or FALSE if bad area (I/Q/M/VM).
	 * 
	 * 		Note:		PDU Reference is expecting as value of SESSION ($_SESSION ['pdu_reference']).
	 * 
	 */
	function f_get_msg_read_bytes ($t_area, $i_addr, $i_count)
	{
		$b_area = true;
		if ($t_area == 'markers')
			$t_area = D_AREA_MARKERS;
		elseif ($t_area == 'inputs')
			$t_area = D_AREA_INPUTS;
		elseif ($t_area == 'outputs')
			$t_area = D_AREA_OUTPUTS;
		elseif ($t_area == 'vm')
			$t_area = D_AREA_DB;
		else
			$b_area = false;
		if ($b_area === true)
		{
			// S7 protocol
			$t_msg_s7_data = "";
			$t_msg_s7_param = D_F_READ_VAR;												// Function
			$t_msg_s7_param .= "\x01";													// Item count
			$t_msg_s7_param .= "\x12";													// ? - Variable specification
			$t_msg_s7_param .= "\x0a";													// ? - Lenght of following address specification
			$t_msg_s7_param .= D_SYNTAX_ID_S7ANY;										// ? - Syntax Id
			$t_msg_s7_param .= D_TS_BYTE_PAR;											// Transport size
			$t_msg_s7_param .= pack ("n", $i_count);									// Lenght
			$t_msg_s7_param .= "\x00\x00";												// DB number
			$t_msg_s7_param .= $t_area;													// Area
			$t_msg_s7_param .= "\x00" . pack ("n", ($i_addr << 3)	);					// Address
			$t_msg_s7_header = D_PROTOCOL_ID;											// Protocol Id
			$t_msg_s7_header .= D_ROSCTR_JOB;											// ROSCTR
			$t_msg_s7_header .= D_RES . D_RES;											// Redundancy Identification (Reserved)
			$t_msg_s7_header .= pack ("n", $_SESSION ['pdu_reference']++);				// Protocol Data Unit Reference
			$t_msg_s7_header .= pack ("n", strlen ($t_msg_s7_param));					// Parameter lenght (2 Bytes)
			$t_msg_s7_header .= pack ("n", strlen ($t_msg_s7_data));					// Data lenght (2 bytes)
			$t_msg_s7 = $t_msg_s7_header . $t_msg_s7_param . $t_msg_s7_data;
			// ISO-COTP
			$t_msg_iso_cotp = D_PDU_TYPE_DT;											// PDU type (DT = DATA)
			$t_msg_iso_cotp .= "\x80";													// ???
			$t_msg_iso_cotp = pack ("C", strlen ($t_msg_iso_cotp)) . $t_msg_iso_cotp;	// Lenght
			// TPKT
			$t_msg_tpkt = D_TPKT_VERSION;												// Version
			$t_msg_tpkt .= D_RES;														// Reserved
			$t_msg_tpkt .= pack ("n", (strlen ($t_msg_iso_cotp . $t_msg_s7) + 4));		// Lenght
			return $t_msg_tpkt . $t_msg_iso_cotp . $t_msg_s7;
		} // if
		return false;
	} // f_get_msg_read_bytes

	

	/*
	 * 
	 * Function for reading all items of one area (inputs/outputs/markers/vm) together - byte by byte, which are finally divide to bits and store in array.
	 * 
	 * 		Input:		Socket
	 * 					Area (possibilities: $_GLOBALS ['area'])
	 * 					Number of items for reading (bits)
	 * 
	 * 		Output:		Array n-attributes with values.
	 * 
	 * 		Notes:		If value was not read, then nothing is stored and bit with actual index will not be exist.
	 * 					Inputs, outputs and markers start index with 1 that is the same counting as in LOGO! Soft Comfort (for example Q1, M2, I3).
	 * 					VM start index with 0, for example V0.6.
	 * 					This function is appropriate for I/Q/M (index of bit is directly number of I/Q/M).
	 * 
	 * 		Examples:	$a_vars [index_of_bit] => $a_vars [15] in case of area I (inputs): I15
	 * 					$a_vars [0] for area outputs (Q) wil not exist see Notes...
	 * 
	 */
	function f_read_bits ($s_socket, $t_area, $i_items)
	{
		if ($t_area == 'vm')
			$i_items *= 8;
		$i_cycles = ceil ($i_items / D_BITS_PER_READ);					// Number of cycles (in one reading (write_socket) can be read maximal 486 bytes)
		if ($i_items > D_BITS_PER_READ)									// Length of result string is higher then 486 bytes and LOGO! can not answer much bytes then 512 (or I don't know how...)
			$i_items_tmp = D_BITS_PER_READ;
		else
			$i_items_tmp = $i_items;
		$i_addr_tmp = 0;
		for ($i_cycle_num = 0; $i_cycle_num < $i_cycles; $i_cycle_num++)// Pass through cycle by cycle with reading max 486 items (bytes) in one cycle
		{
			$t_msg_read = f_get_msg_read_bytes ($t_area, floor ($i_addr_tmp / 8), ceil ($i_items_tmp / 8));	
			socket_write ($s_socket, $t_msg_read, strlen ($t_msg_read));
			$i_return_lenght = (25 + $i_items_tmp);						// Calculating lenght of string dependecy on number of expecting items (bytes)
			$t_return = socket_read ($s_socket, $i_return_lenght);		// $t_return = "\x03\x00\x00\x24\x02\x00\x80\x.....
			$a_return = unpack ("C" . strlen ($t_return), $t_return);	// Convert to chars (1-byte integer) and store in array
			for ($i = 0; $i < $i_items_tmp; $i++)						// Pass through bytes
			{
				for ($j = 0; $j < 8; $j++)								// Pass through bits
				{
					$i_index = ($i_cycle_num * D_BITS_PER_READ) + ($i * 8) + ($j + 1);		// Calculating of variable index dependency on cycle, byte and bit
					if (($i_index <= $i_items) || (($t_area == 'vm') && ($i_index <= ($i_items * 8))))	// Protection against non exist items (for example if LOGO! has only 27 markers, I read 4 bytes (=32 items - bits) even if I have in defs.inc defined only 27. This will ignore non exist bits of last byte.
					{
						if ($t_area == 'vm')							// Index of Q, I, M start from 1, index of VM start from 0
							$i_index--;
						if (isset ($a_return [(26 + $i)]))				// Test of reading data (char on position 26 start data)
							$a_vars [$i_index] = (($a_return [(26 + $i)] & (0b00000001 << $j)) >> $j);	// Save values to array if data were readable
					} // if
				} // for
			} // for
			$i_addr_tmp += D_BITS_PER_READ;								// Increase address for possibility next cycle
			if ($i_items > ($i_addr_tmp + D_BITS_PER_READ))				// Next cycle?
				$i_items_tmp = D_BITS_PER_READ;
			else
				$i_items_tmp = $i_items - ($i_cycle_num * D_BITS_PER_READ) - $i_items_tmp;
		} // for
		return $a_vars;
	} // f_read_bits



	/*
	 * 
	 * Function for reading all items of one area (inputs/outputs/markers/vm) together - byte by byte, which are finally divide to bits and store in array.
	 * 
	 * 		Input:		Socket
	 * 					Area (possibilities: $_GLOBALS ['area'])
	 * 					Number of items for reading (bits)
	 * 
	 * 		Output:		2-dimensional array of items (see Examples below)
	 * 
	 * 		Notes:		If value was not read, then nothing is stored and bit with actual index will not be exist.
	 * 					Inputs, outputs and markers start index with 1 that is the same counting as in LOGO! Soft Comfort (for example Q1, M2, I3).
	 * 					VM start index with 0, for example V0.6.
	 * 					This function is appropriate for VM (array contain separately addres of byte and bit).
	 * 
	 * 		Examples:	$a_vars [index_of byte] [index_of_bit] => $a_vars [15] [5] is for area VM: V15.5
	 * 					$a_vars [4] [5] is for area markers (M): M37 ((4 * 8) + 5) - it is little complicated so for I/Q/M is better using function f_read_bits ()
	 * 					$a_vars [0] [5] for area outputs (Q) will not exist (see Notes)
	 * 
	 */
	function f_read_bits2 ($s_socket, $t_area, $i_items)
	{
		if ($t_area == 'vm')
			$i_items *= 8;
		$i_cycles = ceil ($i_items / D_BITS_PER_READ);					// Number of cycles (in one reading (write_socket) can be read maximal 486 bytes)
		if ($i_items > D_BITS_PER_READ)									// Length of result string is higher then 486 bytes and LOGO! can not answer much bytes then 512 (or I don't know how...)
			$i_items_tmp = D_BITS_PER_READ;
		else
			$i_items_tmp = $i_items;
		$i_addr_tmp = 0;
		for ($i_cycle_num = 0; $i_cycle_num < $i_cycles; $i_cycle_num++)// Pass through cycle by cycle with reading max 486 items (bytes) in one cycle
		{
			$t_msg_read = f_get_msg_read_bytes ($t_area, floor ($i_addr_tmp / 8), ceil ($i_items_tmp / 8));	
			socket_write ($s_socket, $t_msg_read, strlen ($t_msg_read));
			$i_return_lenght = (25 + $i_items_tmp);						// Calculate lenght of string dependecy on number of expecting items (bytes)
			$t_return = socket_read ($s_socket, $i_return_lenght);		// $t_return = "\x03\x00\x00\x24\x02\x00\x80\x.....
			$a_return = unpack ("C" . strlen ($t_return), $t_return);	// Convert to chars (1-byte integer) and store in array
			for ($i = 0; $i < ($i_items_tmp / 8); $i++)					// Pass through bytes
			{
				for ($i_addr_bit = 0; $i_addr_bit < 8; $i_addr_bit++)	// Pass through bits
				{
					$i_index = ($i_cycle_num * D_BITS_PER_READ) + ($i * 8) + ($i_addr_bit + 1);		// Calculating of variable index dependency on cycle, byte and bit
					$i_addr_byte = ($i_cycle_num * D_BYTES_PER_READ) + $i;
					if (($i_index <= $i_items) || (($t_area == 'vm') && ($i_index <= ($i_items * 8))))	// Protection against non exist items (for example if LOGO! has only 27 markers, I read 4 bytes (=32 items - bits) even if I have in defs.inc defined only 27. This will ignore non exist bits of last byte.
					{
						if (isset ($a_return [(26 + $i)]))				// Test of reading data (char on position 26 start data)
							$a_vars [$i_addr_byte] [$i_addr_bit] = (($a_return [(26 + $i)] & (0b00000001 << $i_addr_bit)) >> $i_addr_bit);	// Save values to array if data were readable
					} // if
				} // for
			} // for
			$i_addr_tmp += D_BITS_PER_READ;								// Increase address for possibility next cycle
			if ($i_items > ($i_addr_tmp + D_BITS_PER_READ))				// Next cycle?
				$i_items_tmp = D_BITS_PER_READ;
			else
				$i_items_tmp = $i_items - ($i_cycle_num * D_BITS_PER_READ) - $i_items_tmp;
		} // for
		return $a_vars;
	} // f_read_bits2



	/*
	 * 
	 * Function for reading all of items VM together in bytes.
	 * Technicaly is possible to use this function for reading also inputs-outputs-markers, but I don't see a point.
	 * 
	 * 		Input:		Socket
	 * 					Area (possibilities: $_GLOBALS ['area'])
	 * 					Number of items (bytes)
	 * 
	 * 		Output:		Array with values of bytes.
	 * 
	 * 		Note:		If value was not read, then nothing is saved and byte with actual index will not be exist.
	 * 
	 * 		Examples:	$a_vars [index_of_byte] => $a_vars [12] for area VM contain value V12 (it is V12.0 - V12.7)
	 * 					$a_vars [0] for area outputs (Q) contain values of outputs Q1-Q8, where Q8 have value of most significant bit (MSB). MSB  0  0  0  0  0  0  0  0 x0b
	 * 																																			 Q8 Q7 Q6 Q5 Q4 Q3 Q2 Q1   
	 * 
	 */
	function f_read_bytes ($s_socket, $t_area, $i_items)
	{
		$i_cycles = ceil ($i_items / D_BYTES_PER_READ);					// Number of cycles (in one reading (write_socket) can be read maximal 486 bytes)
		if ($i_items > D_BYTES_PER_READ)								// Length of result string is higher then 486 bytes and LOGO! can not answer much bytes then 512 (or I don't know how...)
			$i_items_tmp = D_BYTES_PER_READ;
		else
			$i_items_tmp = $i_items;
		$i_addr_tmp = 0;
		for ($i_cycle_num = 0; $i_cycle_num < $i_cycles; $i_cycle_num++)// Pass through cycle by cycle with reading max 486 items (bytes) in one cycle
		{
			$t_msg_read = f_get_msg_read_bytes ($t_area, $i_addr_tmp, $i_items_tmp);
			socket_write ($s_socket, $t_msg_read, strlen ($t_msg_read));
			$i_return_lenght = (25 + $i_items_tmp);						// Calculate lenght of string dependecy on number of expecting items (bytes)
			$t_return = socket_read ($s_socket, $i_return_lenght);		// $t_return = "\x03\x00\x00\x24\x02\x00\x80\x.....
			$a_return = unpack ("C" . strlen ($t_return), $t_return);	// Convert to chars (1-byte integer) and store in array
			for ($i = 0; $i < $i_items_tmp; $i++)						// Pass through bytes
			{
				$i_index = ($i_cycle_num * D_BYTES_PER_READ) + $i;
				if ($i_index <= $i_items)								// Protection against non exist items (for example if LOGO! has only 27 markers, I read 4 bytes (=32 items - bits) even if I have in defs.inc defined only 27. This will ignore non exist bits of last byte.
				{
					if (isset ($a_return [(26 + $i)]))					// Test of reading data (char on position 26 start data)
						$a_vars [$i_index] = $a_return [(26 + $i)];		// Save values to array if data were readable
				} // if
			} // for
			$i_addr_tmp += D_BYTES_PER_READ;							// Increase address for possibility next cycle
			if ($i_items > ($i_addr_tmp + D_BYTES_PER_READ))			// Next cycle?
				$i_items_tmp = D_BYTES_PER_READ;
			else
				$i_items_tmp = $i_items - ($i_cycle_num * D_BYTES_PER_READ) - $i_items_tmp;
		} // for
		return $a_vars;
	} // f_read_bytes



 	/*
	 * 
	 * Write value of bit (0 or 1) to LOGO!.
	 * 
	 * 		Input:		Socket
	 * 					Area (possibilities: $_GLOBALS ['area']) 
	 * 					Address of bit
	 * 					Value of bit (0 or 1)
	 * 
	 * 		Output:		True = create string and send request to LOGO! (I don't solve the answer/result)
	 * 					False = bad area
	 * 
	 * 		Notes:		PDU Reference is expecting as value of SESSION ($_SESSION ['pdu_reference']).
	 * 					This function is appropriate for I/Q/M.
	 * 
	 */
	function f_write_bit ($s_socket, $t_area, $i_addr, $i_value)
	{
		$b_area = true;
		if ($t_area == 'markers')
			$t_area = D_AREA_MARKERS;
		elseif ($t_area == 'inputs')
			$t_area = D_AREA_INPUTS;
		elseif ($t_area == 'outputs')
			$t_area = D_AREA_OUTPUTS;
		elseif ($t_area == 'vm')
			$t_area = D_AREA_DB;
		else
			$b_area = false;
		if ($b_area === true)
		{
			// S7 protocol
			$t_msg_s7_data = D_RC_SUCCESS;												// Return code
			$t_msg_s7_data .= D_TS_BIT_DATA;											// Transport size
			$t_msg_s7_data .= pack ("n", strlen ($i_value));							// Lenght
			$t_msg_s7_data .= pack ("C", $i_value);										// Data
			$t_msg_s7_param = D_F_WRITE_VAR;											// Function
			$t_msg_s7_param .= "\x01";													// Item count
			$t_msg_s7_param .= "\x12";													// ? - Variable specification
			$t_msg_s7_param .= "\x0a";													// ? - Lenght of following address specification
			$t_msg_s7_param .= D_SYNTAX_ID_S7ANY;										// ? - Syntax Id
			$t_msg_s7_param .= D_TS_BIT_PAR;											// Transport size
			$t_msg_s7_param .= "\x00\x01";												// Lenght
			$t_msg_s7_param .= "\x00\x00";												// ? - DB number
			$t_msg_s7_param .= $t_area;													// Area
			$t_msg_s7_param .= "\x00" . pack ("n", $i_addr);							// Address
			$t_msg_s7_header = D_PROTOCOL_ID;											// Protocol Id
			$t_msg_s7_header .= D_ROSCTR_JOB;											// ROSCTR
			$t_msg_s7_header .= D_RES . D_RES;											// Redundancy Identification (Reserved)
			$t_msg_s7_header .= pack ("n", $_SESSION ['pdu_reference']++);				// Protocol Data Unit Reference
			$t_msg_s7_header .= pack ("n", strlen ($t_msg_s7_param));					// Parameter lenght (2 Bytes)
			$t_msg_s7_header .= pack ("n", strlen ($t_msg_s7_data));					// Data lenght (2 bytes)
			$t_msg_s7 = $t_msg_s7_header . $t_msg_s7_param . $t_msg_s7_data;
			// ISO-COTP
			$t_msg_iso_cotp = D_PDU_TYPE_DT;											// PDU type
			$t_msg_iso_cotp .= "\x80";													// ???
			$t_msg_iso_cotp = pack ("C", strlen ($t_msg_iso_cotp)) . $t_msg_iso_cotp;	// Lenght
			// TPKT
			$t_msg_tpkt = D_TPKT_VERSION;												// Version
			$t_msg_tpkt .= D_RES;														// Reserved
			$t_msg_tpkt .= pack ("n", (strlen ($t_msg_iso_cotp . $t_msg_s7) + 4));		// Lenght
			$t_msg = $t_msg_tpkt . $t_msg_iso_cotp . $t_msg_s7;
			socket_write ($s_socket, $t_msg, strlen ($t_msg));
			socket_read ($s_socket, 76);
			return true;
		} //if
		return false;
	} // f_write_bit



	/*
	 * 
	 * Write value of bit (0/1) to LOGO!
	 * Address is separately byte and bit.
	 * 
	 * 		Input:		Socket
	 * 					Area (possibilities: $_GLOBALS ['area']) 
	 * 					Address of byte
	 * 					Address of bit
	 * 					Value of bit (0 or 1)
	 * 
	 * 		Output:		True = create string and send request to LOGO! (I don't solve the answer/result)
	 * 					False = bad area
	 * 
	 * 		Notes:		PDU Reference is expecting as value of SESSION ($_SESSION ['pdu_reference']).
	 * 					This function is appropriate for VM.
	 * 
	 */
	function f_write_bit2 ($s_socket, $t_area, $i_addr_byte, $i_addr_bit, $i_value)
	{
		$b_area = true;
		if ($t_area == 'markers')
			$t_area = D_AREA_MARKERS;
		elseif ($t_area == 'inputs')
			$t_area = D_AREA_INPUTS;
		elseif ($t_area == 'outputs')
			$t_area = D_AREA_OUTPUTS;
		elseif ($t_area == 'vm')
			$t_area = D_AREA_DB;
		else
			$b_area = false;
		if ($b_area === true)
		{
			// S7 protocol
			$t_msg_s7_data = D_RC_SUCCESS;												// Return code
			$t_msg_s7_data .= D_TS_BIT_DATA;											// Transport size
			$t_msg_s7_data .= pack ("n", strlen ($i_value));							// Lenght
			$t_msg_s7_data .= pack ("C", $i_value);										// Data
			$t_msg_s7_param = D_F_WRITE_VAR;											// Function
			$t_msg_s7_param .= "\x01";													// Item count
			$t_msg_s7_param .= "\x12";													// ? - Variable specification
			$t_msg_s7_param .= "\x0a";													// ? - Lenght of following address specification
			$t_msg_s7_param .= D_SYNTAX_ID_S7ANY;										// ? - Syntax Id
			$t_msg_s7_param .= D_TS_BIT_PAR;											// Transport size
			$t_msg_s7_param .= "\x00\x01";												// Lenght
			$t_msg_s7_param .= "\x00\x00";												// ? - DB number
			$t_msg_s7_param .= $t_area;													// Area
			$t_msg_s7_param .= "\x00" . pack ("n", (($i_addr_byte << 3) | $i_addr_bit));	// Address
			$t_msg_s7_header = D_PROTOCOL_ID;											// Protocol Id
			$t_msg_s7_header .= D_ROSCTR_JOB;											// ROSCTR
			$t_msg_s7_header .= D_RES . D_RES;											// Redundancy Identification (Reserved)
			$t_msg_s7_header .= pack ("n", $_SESSION ['pdu_reference']++);				// Protocol Data Unit Reference
			$t_msg_s7_header .= pack ("n", strlen ($t_msg_s7_param));					// Parameter lenght (2 Bytes)
			$t_msg_s7_header .= pack ("n", strlen ($t_msg_s7_data));					// Data lenght (2 bytes)
			$t_msg_s7 = $t_msg_s7_header . $t_msg_s7_param . $t_msg_s7_data;
			// ISO-COTP
			$t_msg_iso_cotp = D_PDU_TYPE_DT;											// PDU type
			$t_msg_iso_cotp .= "\x80";													// ???
			$t_msg_iso_cotp = pack ("C", strlen ($t_msg_iso_cotp)) . $t_msg_iso_cotp;	// Lenght
			// TPKT
			$t_msg_tpkt = D_TPKT_VERSION;												// Version
			$t_msg_tpkt .= D_RES;														// Reserved
			$t_msg_tpkt .= pack ("n", (strlen ($t_msg_iso_cotp . $t_msg_s7) + 4));		// Lenght
			$t_msg = $t_msg_tpkt . $t_msg_iso_cotp . $t_msg_s7;
			socket_write ($s_socket, $t_msg, strlen ($t_msg));
			socket_read ($s_socket, 76);
			return true;
		} //if
		return false;
	} // f_write_bit2



	/*
	 * 
	 * Write value few bytes one by one together to area DB0 (VM).
	 * 
	 * 		Input:		Socket
	 * 					Address of first byte (0 - ?).
	 * 					Array with values to write
	 * 
	 * 		Output:		Nothing
	 * 
	 * 		Notes:		This function is appropriate for writing values of type byte, word and dword to area VM.
	 * 					PDU Reference is expecting as value of SESSION ($_SESSION ['pdu_reference']).
	 * 
	 */
	function f_write_bytes ($s_socket, $i_addr, $a_vars)
	{
		$i_lenght = count ($a_vars);
		// S7 protocol
		$t_msg_s7_data = D_RES;														// Return code: Reserved
		$t_msg_s7_data .= D_TS_BYTE_DATA;											// Transport size
		$t_msg_s7_data .= pack ("n", ($i_lenght * 8));								// Lenght: 6 bytes => 48 bits => 0x30
		for ($i = 0; $i < $i_lenght; $i++)
			$t_msg_s7_data .= pack ("C", $a_vars [$i]);								// Data
		$t_msg_s7_param = D_F_WRITE_VAR;											// Function
		$t_msg_s7_param .= "\x01";													// Item count
		$t_msg_s7_param .= "\x12";													// ? - Variable specification
		$t_msg_s7_param .= "\x0a";													// ? - Lenght of following address specification
		$t_msg_s7_param .= D_SYNTAX_ID_S7ANY;										// ? - Syntax Id
		$t_msg_s7_param .= D_TS_BYTE_PAR;											// Transport size
		$t_msg_s7_param .= pack ("n", $i_lenght);									// Lenght: pocet bajtu
		$t_msg_s7_param .= "\x00\x00";												// DB number
		$t_msg_s7_param .= D_AREA_DB;												// Area
		$t_msg_s7_param .= "\x00" . pack ("n", ($i_addr << 3));						// Address
		$t_msg_s7_header = D_PROTOCOL_ID;											// Protocol Id
		$t_msg_s7_header .= D_ROSCTR_JOB;											// ROSCTR
		$t_msg_s7_header .= D_RES . D_RES;											// Redundancy Identification (Reserved)
		$t_msg_s7_header .= pack ("n", $_SESSION ['pdu_reference']++);				// Protocol Data Unit Reference
		$t_msg_s7_header .= pack ("n", strlen ($t_msg_s7_param));					// Parameter lenght (2 Bytes)
		$t_msg_s7_header .= pack ("n", strlen ($t_msg_s7_data));					// Data lenght (2 bytes)
		$t_msg_s7 = $t_msg_s7_header . $t_msg_s7_param . $t_msg_s7_data;
		// ISO-COTP
		$t_msg_iso_cotp = D_PDU_TYPE_DT;											// PDU type
		$t_msg_iso_cotp .= "\x80";													// ???
		$t_msg_iso_cotp = pack ("C", strlen ($t_msg_iso_cotp)) . $t_msg_iso_cotp;	// Lenght
		// TPKT
		$t_msg_tpkt = D_TPKT_VERSION;												// Version
		$t_msg_tpkt .= D_RES;														// Reserved
		$t_msg_tpkt .= pack ("n", (strlen ($t_msg_iso_cotp . $t_msg_s7) + 4));		// Lenght
		$t_msg_write = $t_msg_tpkt . $t_msg_iso_cotp . $t_msg_s7;
		socket_write ($s_socket, $t_msg_write, strlen ($t_msg_write));
		socket_read ($s_socket, 22);
	} // f_write_bytes



	/*
	 * 
	 * Get value (typicaly for VM) of item which is longer then 1 byte (word, dword).
	 * 
	 * 		Input:		Address of byte (0 - ?).
	 * 					Lenght - possibilities:	1 = byte
	 * 											2 = word
	 * 											4 = dword (double word)
	 * 					Array with value of all bytes.
	 * 
	 * 		Output:		Value of item.
	 * 
	 */
	function f_get_value ($i_addr, $i_bytes, $a_vars)
	{
		$i_value = 0;
		for ($i_shift = ($i_bytes - 1); $i_shift >= 0; $i_shift--)
			$i_value += $a_vars [$i_addr++] << ($i_shift * 8);
		return $i_value;
	} // f_get_value



	/*
	 * 
	 * Convert value type word/double word (2/4 bytes) to few one-byte values and them write into LOGO!
	 * 
	 * 		Input:		Socket
	 * 					Address of first byte (0 - ?)
	 * 					Lenght - possibilities:	1 = byte
	 * 											2 = word
	 * 											4 = dword (double word)
	 * 					Values (1 - 4 bytes; 1 byte = 0 - 255; 2 bytes = 0 - 2^16 = 0 - 65536; 4 bytes = 0 - 2^32 = 0 - 4294967296)
	 * 
	 * 		Output:		Nothing
	 * 
	 */
	function f_set_value ($s_socket, $i_addr, $i_bytes, $i_value)
	{
		$i_addr_tmp = 0;
		for ($i_shift = ($i_bytes - 1); $i_shift >= 0; $i_shift--)
			$a_data [$i_addr_tmp++] = ($i_value >> ($i_shift * 8)) & 0b11111111;
		f_write_bytes ($s_socket, $i_addr, $a_data);
	} // f_set_value



?>
