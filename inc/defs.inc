<?php

	$GLOBALS ['logos'] = array (
		0 => array (													// First LOGO!
			'name' => 'A101',											// Name
			'ip' => '192.168.1.13',										// IP address
			'tsap-src' => '10.00',										// Source TSAP - depending on what is set in LOGO! Soft Comfort
			'tsap-dest' => '20.00',										// Destination TSAP - depending on what is set in LOGO! Soft Comfort
			'inputs' => '24',											// Number of inputs
			'outputs' => '20',											// Number of outputs
			'markers' => '64',											// Number of markers
			'vm' => '52'												// Number of VMs, which I want use
		),
		1 => array (													// Second LOGO!
			'name' => 'A102',
			'ip' => '192.168.1.12',
			'tsap-src' => '22.00',
			'tsap-dest' => '21.00',
			'inputs' => '24',
			'outputs' => '16',
			'markers' => '27',
			'vm' => '50'
		)
	);



	/*
	 * 
	 * Constants for functions in func.inc - DON'T EDIT IF YOU DO NOT KNOW EXACTLY WHAT DO YOU DO
	 * 
	 */
	define ('D_LOGO_PORT', '102');										// Port where LOGO! listen for control

	define ('D_RES', "\x00");											// Reserved: 0x00

	// TPKT Protocol
	define ('D_TPKT_VERSION', "\x03");									// Version: 3

	// ISO-COTP (Connection Oriented Transport Protocol)
	define ('D_ISO_COTP', "\x02\xf0\x80");								// Length: 2 (\x02); PDU Type: DT Data (\xf0); Last data unit: Yes (\x80)
	define ('D_PDU_TYPE_CR', "\xe0");									// PDU Type: CR Connect Request
	define ('D_PDU_TYPE_DT', "\xf0");									// PDU Type: DT Data
	define ('D_PDU_TYPE_DR', "\x80");									// PDU Type: DR Disconnect Request
	define ('D_CLASS_00', "\x00");										// Class: 0; Extended formats: False (0x0.), No explicit flow control: False (0x.0)
	define ('D_REASON_NORMAL_DISCONNECT', "\x80");						// Reason: 128 = normal disconnect (RFC 905)
	define ('D_PAR_CODE_TPDU_SIZE', "\xc0");							// Parameter code: tpdu-size
	define ('D_PAR_CODE_SRC_TSAP', "\xc1");								// Parameter code: src-tsap
	define ('D_PAR_CODE_DST_TSAP', "\xc2");								// Parameter code: dst-tsap

	// S7 Protocol - Header
	define ('D_PROTOCOL_ID', "\x32");									// Protocol Id: 0x32;
	define ('D_ROSCTR_JOB', "\x01");									// ROSCTR: Job
	define ('D_ROSCTR_ACK', "\x02");									// ROSCTR: Ack
	define ('D_ROSCTR_ACK_DATA', "\x03");								// ROSCTR: Ack Data
	define ('D_ROSCTR_USERDATA', "\x07");								// ROSCTR: Userdata

	// S7 Protocol - Parameter
	define ('D_F_WRITE_VAR', "\x05");									// Function: Write Var
	define ('D_F_READ_VAR', "\x04");									// Function: Read Var
	define ('D_F_SETUP_COMM', "\xf0");									// Function: Setup Communication
	define ('D_SYNTAX_ID_S7ANY', "\x10");								// Syntax Id: S7ANY
	define ('D_TS_BIT_PAR', "\x01");									// Transport size: BIT (1)
	define ('D_TS_BYTE_PAR', "\x02");									// Transport size: BYTE (2)
	define ('D_TS_CHAR_PAR', "\x03");									// Transport size: CHAR (3)
	define ('D_TS_WORD_PAR', "\x04");									// Transport size: WORD (4)
	define ('D_AREA_P', "\x80");										//Direct peripheral access (P)
	define ('D_AREA_INPUTS', "\x81");									// Area: Inputs (I)
	define ('D_AREA_OUTPUTS', "\x82");									// Area: Outputs (Q)
	define ('D_AREA_MARKERS', "\x83");									// Area: Markers (M)
	define ('D_AREA_DB', "\x84");										// Area: DB (DB)
	define ('D_AREA_DI', "\x85");										// Instance data blocks (DI)
	define ('D_AREA_L', "\x86");										// Local data (L)
	define ('D_AREA_V', "\x87");										// Unknown yet (V)
	define ('D_AREA_CT', "\x1c");										// S7 counters	(S)
	define ('D_AREA_TM', "\x1d");										// S7 timers (T)
	define ('D_AREA_IEC_CT', "\x1e");									// IEC counters (200 family)
	define ('D_AREA_IEC_TM', "\x1f");									// IEC timers (200 family)

	// S7 Protocol - Data
	define ('D_TS_BIT_DATA', "\x03");									// Transport size: BIT (0x03)
	define ('D_TS_BYTE_DATA', "\x04");									// Transport size: BYTE (0x04)
	define ('D_TS_INT_DATA', "\x05");									// Transport size: Integer (0x05)
	define ('D_TS_DINT_DATA', "\x06");									// Transport size: Double integer (0x06)
	define ('D_TS_REAL_DATA', "\x07");									// Transport size: Float (0x07)
	define ('D_RC_SUCCESS', "\xff");									// Return code: Success

	define ('D_BYTES_PER_READ', '486');									// Number of bytes, which LOGO! can send together and I read them by socket_read (apparently limited by a length of 512 bytes - include header)
	define ('D_BITS_PER_READ', '3888');									// Number of bits, which LOGO! can send together and I read them by socket_read (apparently limited by a length of 512 bytes - include header)

	$GLOBALS ['area'] = array (0 => 'inputs', 1 => 'outputs', 2 => 'markers', 3 => 'vm');	// Array of possibilities for AREA

?>
