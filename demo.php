<?php

	session_start ();

	require 'inc/defs.inc';
	require 'inc/func.inc';

	echo "<!DOCTYPE html>";
	echo "\n<html lang=\"cs\">";
	echo "\n<head>";
	echo "\n\t<meta charset=\"utf-8\">";
	echo "\n\t<title>D&nbsp;E&nbsp;M&nbsp;O&nbsp;(v" . D_VERSION . ")</title>";
	echo "\n\t<link rel=\"stylesheet\" type=\"text/css\" href=\"css/main.css\" />";
	echo "\n</head>";
	echo "\n<body>";

	$b_toggle = false;													// Flag, if bit of VM after change to 1 will be set automaticaly back to 0
	$b_wait = false;													// Flag, that bit of VM was changes to 1



	/*
	 * Test if exist/set variable PDU reference and Source Reference for network protocol
	 */
	if ((isset ($_SESSION ['pdu_reference'])) && ($_SESSION ['pdu_reference'] < 64000))
		$_SESSION ['pdu_reference']++;
	else
		$_SESSION ['pdu_reference'] = 1;
	if ((isset ($_SESSION ['src_reference'])) && ($_SESSION ['src_reference'] < 64000))
		$_SESSION ['src_reference']++;
	else
		$_SESSION ['src_reference'] = 1;



	/*
	 * Create socket to all LOGO!s and setting communication
	 */
	$a_socket = f_connect_logos ();



	/*
	 * 
	 * User set ID for change bit of any item or change value of any item
	 * 
	 */
	if ((isset ($_GET ['id'])) && ($_GET ['id'] == D_ID_CHANGE_ITEM))
	{

		/*
		 * Test user inputs ($_GET)
		 */
		$b_logo = false;
		$b_area = false;
		$b_addr = false;
		$b_addr2 = false;
		$b_val = false;
		if ((isset ($_GET ['logo'])) && (is_int (intval ($_GET ['logo']))) && ($_GET ['logo'] < (count ($GLOBALS ['logos']))))
			$b_logo = true;
		if (isset ($_GET ['area']))
		{
			foreach ($GLOBALS ['area'] as $t_tmp_area)					// Pass through all values from $GLOBALS ['area']
			{
				if ($t_tmp_area == $_GET ['area'])						// Value $_GET ['area'] is among $GLOBALS ['area']?
					$b_area = true;
			} // foreach
		} // if
		if ((isset ($_GET ["addr"])) && (is_int (intval ($_GET ["addr"]))) && ($_GET ["addr"] < 65536))	// 0 - 2^16 (2 bytes, LOGO! can read 3 bytes as address, but I don't know how to convert it ...)
			$b_addr = true;
		if ((isset ($_GET ["val"])) && (is_int (intval ($_GET ["val"]))))// Integer > 0
			$b_val = true;
		if ((isset ($_GET ["addr1"])) && (isset ($_GET ["addr2"])) && (is_int (intval ($_GET ["addr1"]))) && (is_int (intval ($_GET ["addr2"]))) && ($_GET ["addr1"] < 65536) && ($_GET ["addr2"] < 65536))	// 0 - 2^16 (2 bytes, LOGO! can read 3 bytes as address, but I don't know how to convert it ...)
			$b_addr2 = true;
		

		/*
		 * Users inputs are OK - user want change any bit of item 
		 */
		if (($b_logo === true) && ($b_area === true) && ($b_addr === true) && ($b_val === true))
		{
			$i_logo_num = $_GET ['logo'];
			$t_area = $_GET ['area'];
			$i_item_addr = $_GET ['addr'];
			if ($t_area == 'vm')
			{
				$b_wait = true;
				$i_item_addr++;											// Increment address of VM, because few lines below I will decrement it ,-) Only VM LOGO! counting from 0.
			} // if
			if ($a_socket [$i_logo_num] !== false)						// Socket OK?
				f_write_bit ($a_socket [$i_logo_num], $t_area, ($i_item_addr - 1), $_GET ['val']);	// Decrement address, because I counting from 1 but addresses start with 0
		} // if

		/*
		 * Users inputs OK - user want change any bit of item (adress is send separately byte and bit - typicaly for VM)
		 */
		elseif (($b_logo === true) && ($b_area === true) && ($b_addr2 === true) && ($b_val === true))
		{
			$i_logo_num = $_GET ['logo'];
			$t_area = $_GET ['area'];
			$i_addr_byte = $_GET ['addr1'];
			$i_addr_bit = $_GET ['addr2'];
			if ($a_socket [$i_logo_num] !== false)						// Socket OK?
				f_write_bit2 ($a_socket [$i_logo_num], $t_area, $i_addr_byte, $i_addr_bit, $_GET ['val']);	// Decrement address, because I counting from 1 but addresses start with 0
		} // if



		/*
		 * User want change value of any item
		 */
		elseif (isset ($_POST ['sent']))
		{
			$i_logo_num = $_GET ['logo'];
			f_set_value ($a_socket [$i_logo_num], $_POST ['addr'], $_POST ['bytes'], $_POST ['value']);
		} // elseif

		/*
		 * User inputs were wrong
		 */
		else
			echo "\n\t<br />Wrong users inputs...";

	} // if



	/*
	 * User want write date and time to LOGO1
	 */
	if ((isset ($_GET ['id'])) && ($_GET ['id'] == D_ID_CHANGE_DATETIME) && (isset ($_GET ['logo'])) && (is_int (intval ($_GET ['logo']))) && ($_GET ['logo'] < (count ($GLOBALS ['logos']))))
	{
		$i_logo_num = $_GET ['logo'];
		$a_datetime = array ('year' => $_POST ['year'], 'month' => $_POST ['month'], 'day' => $_POST ['day'], 'hour' => $_POST ['hour'], 'minute' => $_POST ['minute'], 'second' => $_POST ['second']);
		if ($a_socket [$i_logo_num] !== false)							// Socket OK?
			f_write_datetime ($a_socket [$i_logo_num], $a_datetime);
	} // if



	/*
	 * Read variables
	 * 
	 * Read variables are stored in 3/4-dimensional array: $a_vars [logo_number] [input/output/marker/vm/vm_by_bytes] [address_of_bit/byte] ([address_of_bit] - only for VM, what are reading by function f_read_bits2)
	 * If variable doesn't exist, then it wasn't succeed of reading from LOGO!.
	 * 
	 * Examples:	Value Q15 LOGO! 4 is stored in $a_vars [4] ['outputs'] [15]
	 * 				Value V0.6 LOGO! 3 is stored in  $a_vars [3] ['V'] [0] [6]
	 * 				Value V35.3 LOGO! 0 is stored in  $a_vars [0] ['V'] [35] [3]
	 * 				Value of byte V44 LOGO! 5 is stored in $a_vars [5] ['VB'] [44]
	 *	 
	 */
	$i_logo_num = 0;
	foreach ($GLOBALS ["logos"] as $a_logo)
	{
		if ($a_socket [$i_logo_num] !== false)																		// Socket OK?
		{
			$a_vars [$i_logo_num] ['I'] = f_read_bits ($a_socket [$i_logo_num], 'inputs', $a_logo ['inputs']);		// Inputs bits
			$a_vars [$i_logo_num] ['Q'] = f_read_bits ($a_socket [$i_logo_num], 'outputs', $a_logo ['outputs']);	// Outputs bits
			$a_vars [$i_logo_num] ['M'] = f_read_bits ($a_socket [$i_logo_num], 'markers', $a_logo ['markers']);	// Markers bits
			$a_vars [$i_logo_num] ['V'] = f_read_bits2 ($a_socket [$i_logo_num], 'vm', $a_logo ['vm']);				// VM bits
			$a_vars [$i_logo_num] ['VB'] = f_read_bytes ($a_socket [$i_logo_num], 'vm', $a_logo ['vm']);			// VM bytes
			$a_date [$i_logo_num] = f_read_datetime ($a_socket [$i_logo_num]);										// Date and time
		} // if
		$i_logo_num++;
	} // foreach



	// Link to site without attributs in address bar
	echo "\n\t<a href=\"{$_SERVER ["PHP_SELF"]}\" class=\"B\">Index (clean address bar)</a><br /><br />";



	// Cycle for all of LOGO!s from $GLOBALS ['logos']
	for ($i_logo_num = 0; $i_logo_num < count ($GLOBALS ['logos']); $i_logo_num++)
	{

		if ($a_socket [$i_logo_num] !== false)							// Socket OK?
		{

			/*
			 * Extract name and IP address LOGO!
			 */
			echo "\n\t<p><span class=\"Name\">{$GLOBALS ['logos'] [$i_logo_num] ['name']}</span>&nbsp;<span class=\"Ip\">{$GLOBALS ['logos'] [$i_logo_num] ['ip']}</span></p>";



			/*
			 * Extract date and time of LOGO! + form for edit
			 */
			echo "\n\t<p id=\"l{$i_logo_num}d\"><span class=\"Head\">Date and time:</span>&nbsp;<span class=\"Info\">{$a_date [$i_logo_num] ['day']}. {$GLOBALS ['month'] [$a_date [$i_logo_num] ['month']]} " . (2000 + $a_date [$i_logo_num] ['year']) . " - ";
			if ($a_date [$i_logo_num] ['hour'] < 10)
				echo "0";
			echo "{$a_date [$i_logo_num] ['hour']}:";
			if ($a_date [$i_logo_num] ['minute'] < 10)
				echo "0";
			echo "{$a_date [$i_logo_num] ['minute']}:";
			if ($a_date [$i_logo_num] ['second'] < 10)
				echo "0";
			echo "{$a_date [$i_logo_num] ['second']}</span></p>";
			echo "\n\t<p>";
			echo "\n\t\t<form name=\"DateTime\" method=\"post\" action=\"?id=" . D_ID_CHANGE_DATETIME . "&amp;logo={$i_logo_num}#l{$i_logo_num}d\">";
			echo "\n\t\t\t<select name=\"day\">";
			for ($i_day = 1; $i_day <= 31; $i_day++)
			{
				echo "\n\t\t\t\t<option value=\"{$i_day}\"";
				if ($i_day == date ('j'))
					echo " selected";
				echo ">{$i_day}</option>";
			} // for
			echo "\n\t\t\t</select>";
			echo "\n\t\t\t<select name=\"month\">";
			for ($i_month = 1; $i_month <= 12; $i_month++)
			{
				echo "\n\t\t\t\t<option value=\"{$i_month}\"";
				if ($i_month == date ('n'))
					echo " selected";
				echo ">{$GLOBALS ['month'] [$i_month]}</option>";
			} // for
			echo "\n\t\t\t</select>";
			echo "\n\t\t\t<select name=\"year\">";
			for ($i_year = 17; $i_year <= 27; $i_year++)
			{
				echo "\n\t\t\t\t<option value=\"{$i_year}\"";
				if ($i_year == date ('y'))
					echo " selected";
				echo ">" . (2000 + $i_year) . "</option>";
			} // for
			echo "\n\t\t\t</select>&nbsp;&nbsp;-&nbsp;";
			echo "\n\t\t\t<select name=\"hour\">";
			for ($i_hour = 0; $i_hour <= 24; $i_hour++)
			{
				echo "\n\t\t\t\t<option value=\"{$i_hour}\"";
				if ($i_hour == date ('G'))
					echo " selected";
				echo ">{$i_hour}</option>";
			} // for
			echo "\n\t\t\t</select>&nbsp;:";
			echo "\n\t\t\t<select name=\"minute\">";
			for ($i_minute = 0; $i_minute <= 59; $i_minute++)
			{
				echo "\n\t\t\t\t<option value=\"{$i_minute}\"";
				if ($i_minute == date ('i'))
					echo " selected";
				echo ">{$i_minute}</option>";
			} // for
			echo "\n\t\t\t</select>&nbsp;:";
			echo "\n\t\t\t<select name=\"second\">";
			for ($i_second = 0; $i_second <= 59; $i_second++)
			{
				echo "\n\t\t\t\t<option value=\"{$i_second}\"";
				if ($i_second == date ('s'))
					echo " selected";
				echo ">{$i_second}</option>";
			} // for
			echo "\n\t\t\t</select>";
			echo "\n\t\t\t&nbsp;&nbsp;&nbsp;<input type=\"submit\" value=\"Write\" />";
			echo "\n\t\t</form>";
			echo "\n\t</p>";



			/*
			 * Extract concrete VM + form for edit
			 */
			echo "\n\t<p id=\"l{$i_logo_num}vm1\">";
			$b_addr = false;
			$b_bytes = false;
			if ((isset ($_POST ['addr'])) && (is_int (intval ($_POST ['addr']))) && ($_POST ['addr'] < 65536))	// 0 - 2^16 (2 bytes, LOGO! can read 3 bytes as address, but I don't know how to convert it ...)
				$b_addr = true;
			if ((isset ($_POST ['bytes'])) && (is_int (intval ($_POST ['bytes']))))		// integer > 0
				$b_bytes = true;
			if (($b_addr === true) && ($b_bytes === true))
			{
				$i_value = f_get_value ($_POST ['addr'], $_POST ['bytes'], $a_vars [$i_logo_num] ['VB']);
				echo "\n\t\t<span class=\"Head\">VM{$_POST ['addr']}&nbsp;=&nbsp;</span>&nbsp;<span class=\"Info\">{$i_value}";
			} // if
			echo "\n\t\t<form name=\"OneVMR\" method=\"post\" action=\"?r=1&amp;#l{$i_logo_num}vm1\">";
			echo "\n\t\t<span class=\"Head\">VM</span>";
			echo "\n\t\t\t<select name=\"addr\">";
			for ($i_addr = 0; $i_addr <= $GLOBALS ['logos'] [$i_logo_num] ['vm']; $i_addr++)
			{
				echo "\n\t\t\t\t<option value=\"{$i_addr}\"";
				if ((isset ($_POST ['addr'])) && ($_POST ['addr'] == $i_addr))
					echo " selected";
				echo ">{$i_addr}</option>";
			} // for
			echo "\n\t\t\t</select>";
			echo "\n\t\t<span class=\"Head\">Lenght:</span>";
			echo "\n\t\t\t<select name=\"bytes\">";
			for ($i = 0; $i < count ($GLOBALS ['bytes']); $i++)
			{
				echo "\n\t\t\t\t<option value=\"{$GLOBALS ['bytes'] [$i] ['lenght']}\"";
				if ((isset ($_POST ['bytes'])) && ($_POST ['bytes'] == $GLOBALS ['bytes'] [$i] ['lenght']))
					echo " selected";
				echo ">{$GLOBALS ['bytes'] [$i] ['name']}</option>";
			} // for
			echo "\n\t\t\t</select>";
			echo "\n\t\t\t<button type=\"submit\">Load</button>";
			echo "\n\t\t</form>";

			echo "\n\t\t<form name=\"OneVMW\" method=\"post\" action=\"?id=" . D_ID_CHANGE_ITEM . "&amp;logo={$i_logo_num}#l{$i_logo_num}vm1\">";
			echo "\n\t\t<span class=\"Head\">VM</span>";
			echo "\n\t\t\t<select name=\"addr\">";
			for ($i_addr = 0; $i_addr <= $GLOBALS ['logos'] [$i_logo_num] ['vm']; $i_addr++)
			{
				echo "\n\t\t\t\t<option value=\"{$i_addr}\"";
				if ((isset ($_POST ['addr'])) && ($_POST ['addr'] == $i_addr))
					echo " selected";
				echo ">{$i_addr}</option>";
			} // for
			echo "\n\t\t\t</select>";
			echo "\n\t\t<span class=\"Head\">Lenght:</span>";
			echo "\n\t\t\t<select name=\"bytes\">";
			for ($i = 0; $i < count ($GLOBALS ['bytes']); $i++)
			{
				echo "\n\t\t\t\t<option value=\"{$GLOBALS ['bytes'] [$i] ['lenght']}\"";
				if ((isset ($_POST ['bytes'])) && ($_POST ['bytes'] == $GLOBALS ['bytes'] [$i] ['lenght']))
					echo " selected";
				echo ">{$GLOBALS ['bytes'] [$i] ['name']}</option>";
			} // for
			echo "\n\t\t\t</select>";
			echo "\n\t\t<span class=\"Head\">Value:</span>";
			echo "\n\t\t\t<input type=\"text\" name=\"value\" size=\"10\" value=\"";
			if (isset ($_POST ['value']))
				echo "{$_POST ['value']}";
			else
				echo "0";
			echo "\" />";
			echo "\n\t\t\t<input type=\"hidden\" name=\"sent\" value=\"1\" />";
			echo "\n\t\t\t<input type=\"submit\" value=\"Save\" />";
			echo "\n\t\t</form>";



			/*
			 * States of inputs (I)
			 */
			echo "\n\t<p id=\"l{$i_logo_num}i\"><span class=\"Head\">States of inputs (not possible to control):</span></p>";
			echo "\n\t<div class=\"Items\">";
			for ($i = 0; $i < ceil ($GLOBALS ['logos'] [$i_logo_num] ['inputs'] / 10); $i++)		// Row have 10 cols => number of lines = (number of inputs / 10) rounded up
			{
				echo "\n\t\t";
				for ($j = 1; $j <= 10; $j++)														// Individual cells
				{
					$i_index = ($i * 10) + $j;
					if ($i_index <= $GLOBALS ['logos'] [$i_logo_num] ['inputs'])					// Last row must not be full of cells so I test index here
					{
						if (isset ($a_vars [$i_logo_num] ['I'] [$i_index]))							// Exists value? (Answered LOGO! any value?)
							echo "<div class=\"V{$a_vars [$i_logo_num] ['I'] [$i_index]}\">I{$i_index}</div>";
						else
							echo "<div class=\"Err\">I{$i_index}</div>";
					} // if
				} // for
				echo "<br />";
			} // for
			echo "\n\t</div>";



			/*
			 * States of outputs (Q)
			 */
			echo "\n\t<p id=\"l{$i_logo_num}o\"><span class=\"Head\">States of outputs (possible to control outputs which is not control by LOGO! program):</span></p>";
			echo "\n\t<div class=\"Items\">";
			for ($i = 0; $i < ceil ($GLOBALS ['logos'] [$i_logo_num] ['outputs'] / 10); $i++)
			{
				echo "\n\t\t";
				for ($j = 1; $j <= 10; $j++)
				{
					$i_index = ($i * 10) + $j;
					if ($i_index <= $GLOBALS ['logos'] [$i_logo_num] ['outputs'])
					{
						if (isset ($a_vars [$i_logo_num] ['Q'] [$i_index]))
						{
							if ($a_vars [$i_logo_num] ['Q'] [$i_index] == 0)
								$i_new_value = 1;
							else
								$i_new_value = 0;
							echo "<a href=\"?id=" . D_ID_CHANGE_ITEM . "&amp;logo={$i_logo_num}&amp;area=outputs&amp;addr={$i_index}&amp;val={$i_new_value}#l{$i_logo_num}o\" class=\"V{$a_vars [$i_logo_num] ['Q'] [$i_index]}\">Q{$i_index}</a>";
						} // if
						else
							echo "<div class=\"Err\">Q{$i_index}</div>";
					} // if
				} // for
				echo "<br />";
			} // for
			echo "\n\t</div>";
			echo "\n\t<br /><a href=\"{$_SERVER ["PHP_SELF"]}#l{$i_logo_num}o\" class=\"B\">Index (clean address bar and anchoring by outputs)</a>";



			/*
			 * States of markers (M)
			 */
			echo "\n\t<p id=\"l{$i_logo_num}m\"><span class=\"Head\">States of markers (possible to control):</span></p>";
			echo "\n\t<div class=\"Items\">";
			for ($i = 0; $i < ceil ($GLOBALS ['logos'] [$i_logo_num] ['markers'] / 10); $i++)
			{
				echo "\n\t\t";
				for ($j = 1; $j <= 10; $j++)
				{
					$i_index = ($i * 10) + $j;
					if ($i_index <= $GLOBALS ['logos'] [$i_logo_num] ['markers'])
					{
						if (isset ($a_vars [$i_logo_num] ['M'] [$i_index]))
						{
							if ($a_vars [$i_logo_num] ['M'] [$i_index] == 0)
								$i_new_value = 1;
							else
								$i_new_value = 0;
							echo "<a href=\"?id=" . D_ID_CHANGE_ITEM . "&amp;logo={$i_logo_num}&amp;area=markers&amp;addr={$i_index}&amp;val={$i_new_value}#l{$i_logo_num}m\" class=\"V{$a_vars [$i_logo_num] ['M'] [$i_index]}\">M{$i_index}</a>";
						} // if
						else
							echo "<div class=\"Err\">M{$i_index}</div>";
					} // if
				} // for
				echo "<br />";
			} // for
			echo "\n\t</div>";
			echo "\n\t<br /><a href=\"{$_SERVER ["PHP_SELF"]}#l{$i_logo_num}m\" class=\"B\">Index (clean address bar and anchoring by markers)</a>";


			/*
			 * States of VM - by bytes
			 */
			echo "\n\t<p id=\"l{$i_logo_num}vmB\"><span class=\"Head\">Values of VM (not possible to control, only values by bytes):</span></p>";
			echo "\n\t<div class=\"Items\">";
			for ($i = 0; $i < ($GLOBALS ['logos'] [$i_logo_num] ['vm'] / 10); $i++)
			{
				echo "\n\t\t";
				for ($j = 1; $j <= 10; $j++)
				{
					$i_index = ($i * 10) + $j - 1;
					if ($i_index < $GLOBALS ['logos'] [$i_logo_num] ['vm'])
					{
						if (isset ($a_vars [$i_logo_num] ['VB'] [$i_index]))
						{
							if ($a_vars [$i_logo_num] ['VB'] [$i_index] == 0)
								$t_class = "V0";
							else
								$t_class = "V1";
							echo "<div class=\"{$t_class}\">V{$i_index}={$a_vars [$i_logo_num] ['VB'] [$i_index]}</div>";
						} // if
						else
							echo "<div class=\"Err\">V{$i_index}</div>";
					} // if
				} // for
				echo "<br />";
			} // for
			echo "\n\t</div>";
			echo "\n\t<br /><a href=\"{$_SERVER ["PHP_SELF"]}#l{$i_logo_num}vmB\" class=\"B\">Index (clean address bar and anchoring by VM by bytes)</a>";


			/*
			 * States of VM - by bits
			 */
			echo "\n\t<p id=\"l{$i_logo_num}vm\"><span class=\"Head\">States of VM (possible to control):</span></p>";
			echo "\n\t<div class=\"Items\">";
			for ($i_addr_byte = 0; $i_addr_byte < $GLOBALS ['logos'] [$i_logo_num] ['vm']; $i_addr_byte++)
			{
				echo "\n\t\t";
				for ($i_addr_bit = 0; $i_addr_bit < 8; $i_addr_bit++)
				{
					if (isset ($a_vars [$i_logo_num] ['V'] [$i_addr_byte] [$i_addr_bit]))
					{
						if ($a_vars [$i_logo_num] ['V'] [$i_addr_byte] [$i_addr_bit] == 0)
							$i_new_value = 1;
						else
							$i_new_value = 0;
						echo "<a href=\"?id=" . D_ID_CHANGE_ITEM . "&amp;logo={$i_logo_num}&amp;area=vm&amp;addr1={$i_addr_byte}&amp;addr2={$i_addr_bit}&amp;val={$i_new_value}#l{$i_logo_num}vm\" class=\"V{$a_vars [$i_logo_num] ['V'] [$i_addr_byte] [$i_addr_bit]}\">V{$i_addr_byte}.{$i_addr_bit}</a>";
					} // if
					else
						echo "<div class=\"Err\">V{$i_addr_byte}.{$i_addr_bit}</div>";
				} // for
				echo "<br />";
			} // for
			echo "\n\t</div>";
			echo "\n\t<br /><a href=\"{$_SERVER ["PHP_SELF"]}#l{$i_logo_num}vm\" class=\"B\">Index (clean address bar and anchoring by VM by bits)</a>";


		} // if
	
	} // for

	

	echo "\n</body>";
	echo "\n</html>";



	/*
	 * Toggle VM back to 0 if user wanted change state to 1 (pulse instead permanently 1)
	 */
	if (($b_toggle === true) && ($b_wait === true))											// Flag for toggle and flag for pulse OK?
	{
		usleep (200000);																	// Wait for 200000 us = 200ms (200ms long puls in logical 1)
		if ($a_socket [$_GET ['logo']])														// Socket OK?
		{
			$t_msg_write = f_get_msg_write_item ('vm', $_GET ['addr'], 0);					// String for write 0 to changed bit
			socket_write ($a_socket [$_GET ['logo']], $t_msg_write, strlen ($t_msg_write));	// Write 0 to changed bit (request)
			socket_read ($a_socket [$_GET ['logo']], 76);
		} // if
		$b_wait = false;																	// Toggle flag "toggle"
	} // if



	/*
	 * Disconnect socket
	 */
	f_disconnect_logos ($a_socket);

?>
