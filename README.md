# LogoPHP

PHP functions for control PLC Siemens Logo! 7 & 8

Control means exactly:
- read state (0/1) of inputs, outputs
- set state (0/1) of outputs which are not used in program of LOGO!
- read/set bit of markers and VMs (0/1)
- read/set value of markers and VMs (1,2,4 bytes long values)
- read/set date and time of LOGO! version 8 (0BA8)

### Requirements

[**LOGO! Soft Comfort**](https://w3.siemens.com/mcms/programmable-logic-controller/en/logic-module-logo/demo-software/pages/default.aspx) for LOGO! program/project edit (set TSAP, see below)
<br />__PHP__ (tested on 7.0) for controling LOGO! by script (need session and socket functions).
<br />__Web server__ (nginx, apache, ...) with php module for LOGO! control by web site (for example demo page).

### Files

- Demo page needs all files from directories **_css/_**, **_inc/_** and file **_demo.php_**.
- Needed files for your own scripts are only **_inc/defs.inc_** and **_inc/func.inc_**. Others you can ignore or delete.

### Installation

Copy required files.

### Configuration
Edit upper part of **_inc/defs.inc_**.
<br />There are variables called **_$GLOBALS ['logos']_**. They comprise details of all LOGO!s what you want to control (like IP address, amount of inputs, outputs, ...).
<br />For example:
```
	$GLOBALS ['logos'] = array (
		0 => array (                 // First LOGO!
			'name' => 'A101',        // Name
			'ip' => '192.168.1.13',  // IP address
			'tsap-src' => '10.00',   // Source TSAP - depending on what is set in LOGO! Soft Comfort
			'tsap-dest' => '20.00',  // Destination TSAP - depending on what is set in LOGO! Soft Comfort
			'inputs' => '24',        // Number of inputs
			'outputs' => '20',       // Number of outputs
			'markers' => '64',       // Number of markers
			'vm' => '52'             // Number of VMs, which I want use
		),
		1 => array (                 // Second LOGO!
			'name' => 'A102',
			'ip' => '192.168.1.12',
			'tsap-src' => '22.00',
			'tsap-dest' => '21.00',
			'inputs' => '24',
			'outputs' => '16',
			'markers' => '27',
			'vm' => '50'
		)
	);
```
<br />Lines with **_tsap-src_** a **_tsap-dest_** must correspond to information which are stated in the software **_LOGO! Soft Comfort_**.
<br />I mean that LOGO! version 7 had problem with communication when TSAP was not set correctly. I did not have such kind of problem with version 8.
<br />Settings in **_LOGO! Soft Comfort_**:
<br />![LOGO! Soft Comfort - Add Server connection](img/logocomfort-1.png)
<br />![LOGO! Soft Comfort - TSAP](img/logocomfort-2.png)
<br />![LOGO! Soft Comfort - Result](img/logocomfort-3.png)
<br />Write your own php script with functions from **_inc/func.inc_** or test **_demo.php_** on your web server ;-)

### Functions description
Function **f_connect_logos ()** creates socket and setup communication with LOGO!.
- Call for example: _$a_socket = f_connect_logos ();_
- Then you can _$a_socket [logo_number]_ when you want to use socket.

Function **f_disconnect_logos ()** finishes socket with LOGO!.
- Call for example _f_disconnect_logos ($a_socket [logo_number])_.

Function **f_read_datetime ($s_socket)** read date and time from one LOGO (works only with version 8 - 0BA8).
- The result is saved to array with 6 elements with keys: 'year', 'month', 'day', 'hour', 'minute' a 'second'.
- If you want to get date and time from LOGO! number 5 you can use: _$a_date = f_read_datetime ($a_socket [5]);_
- Then time is saved in variables _$a_date ['hour']_, _$a_date ['minute']_ and _$a_date ['second']_.
- It is similar for getting the date.
	
Function **f_write_datetime ($s_socket, $a_datetime)** writes date and time to LOGO! (it works only with version 8 - 0BA8).
- Firstly prepare array with 6 elements with keys: 'year', 'month', 'day', 'hour', 'minute' a 'second'.
- For example like this: _$a_datetime = array ('year' => 18, 'month' => 12, 'day' => 9, 'hour' => 20, 'minute' => 0, 'second' => 0);_
- Or you can use php function for time and save actual time from computer: _$a_datetime = array ('year' => date ('y'), 'month' => date ('n'), 'day' => date ('j'), 'hour' => date ('G'), 'minute' => date ('i'), 'second' => date ('s'));_
- And then save it to LOGO! with function: _f_write_datetime ($a_socket [2], $a_datetime);_ in this example it saves actual date and time to LOGO! with index 2 in _$GLOBALS ['logos']_.

Read values bits one by one (inputs, outputs, markers) is possible by function **f_read_bits ($s_socket, $t_area, $i_items)**.
- For example for reading states of 24 inputs of LOGO! with index 0 you can use: _$a_in = f_read_bits ($a_socket [0], 'inputs', 24)_.
- Amount of items is in bits so result is array _$a_in [1]_ - _$a_in [24]_.
- Got values are 0 / 1 or element doesn't exist (if it is not possible to read).

Read values bits one by one (VMs) is possible by function **f_read_bits2 ($s_socket, $t_area, $i_items)**.
- For example for reading values of 100 VMs from LOGO! with index 1 you can use: _$a_vm = f_read_bits ($a_socket [1], 'vm', 100)_.
- Amount of items is in bytes so result is array _$a_vm [0] [0]_ - _$a_vm [99] [7]_ (in LOGO! Soft Comfort like V0.0 - V99.7)
- Got values are 0 / 1 or element doesn't exist (if it is not possible to read).
- For example V34.6 you can read as _$a_vm [34] [6]_.

Read values bytes one by one is possible by function **f_read_bytes ($s_socket, $t_type, $i_items)**.
- For example for getting first 100 VMs of LOGO! with index 0 by bytes you can use: _$a_vmsB = f_read_bytes ($a_socket [0], 'vm', 100).
- Index starts with 0 so values will be saved in _$a_vmsB [0]_ - _$a_vmsB [99]_ (in LOGO! Soft Comfort like VM0 - VM99)
- For example value VM12 (with length 1 byte) is saved in variable _$a_vmsB [12]_.
- Got values are in range 0 - 255 or element doesn't exist (if it is not possible to read)
- VMs are in LOGO! in range 0-850.

If there is any value of more bytes (word, dword), you can get to this value with the function **f_get_value ($i_addr, $i_bytes, $a_vars)**.
- For example VM44 will be of “word” length (2 bytes).
- With using the function _$a_vms = f_read_bytes ($s_socket, 'vm', '100')_ you can get the array of values with indexes 0 – 99 (_$a_vms [0]_ - _$a_vms [99]_) (in LOGO! Soft Comfort such as VM0 - VM99)
- The value VM44 should be divided in _$a_vms [44]_ and _$a_vms [45]_.
- For value VM44=1000 should be _$a_vms [44] = 3_ and _$a_vms [45] = 232_
- Then call _$i_value = f_get_value (44, 2, $a_vms)_.
- And in the variable _$i_value_ the value 1000 will be saved.

For writing value of one or more bytes (byte, word, dword) you can use function **f_set_value ($s_socket, $i_addr, $i_bytes, $i_value)**.
- Basically this is used for writing one VM, so I do not check if the LOGO! enter more values than 415 (I expect that this is the amount of values that the LOGO! is able to write at the same time).
- For example if you want to write VM44 with length dword (4 bytes) and value 99999, then you can use function _f_set_value ($s_socket, 44, 4, 99999)_.

For writing one bit you can use function **f_write_bit ($s_socket, $t_area, $i_addr, $i_value)**.
- For example if you want to set marker M12 to the value 0 you can use _f_write_bit ($s_socket, 'markers', 12, 0)_

For writing one VM bit is better to use function **f_write_bit2 ($s_socket, $t_area, $i_addr_byte, $i_addr_bit, $i_value)**.
- It is not necessary to check what kind of byte it is but you can directly write it e.g. for writing value 1 to V34.4 you can use: _f_write_bit2 ($s_socket, 'vm', 34, 4, 1)_.

Other functions (**f_get_msg_connect ($t_tsap_src, $t_tsap_dest)**, **f_get_msg_setup ()**, **f_get_msg_read_bytes ($t_area, $i_addr, $i_count)** and **f_write_bytes ($s_socket, $i_addr, $a_vars)**) are internal functions for above mentioned.

### Notes
Your script must set variables **_$_SESSION ['src_reference']_** and **_$_SESSION ['pdu_reference']_**.

TCP/IP protocol need to define **_Source Reference_** number by every making connection. This number is everytime different.

**_PDU Reference_** is number what is different by every read/write socket function.

I didn't found better choice for saving this variables then **_$_SESSION_**. In **_demo.php_** I create it once and then I only increment it. Variables are reseting by finish session (typically close browser).

These variables must have value 0-65535. I don't control this anywhere.

**_TSAP_**:
- **_tsap-src_** is TSAP of computer with PHP script
- **_tsap-dest_** is TSAP of LOGO!
- Both is possible to see in **_LOGO! Soft Comfort_** -> _Network Project_ and press right mouse button on LOGO! sign -> _Add Server Connection_ - left TSAP is **_tsap-dest_** and right TSAP is possible to set - **_tsap-src_**.
- Maybe is possible to have everywhere the same **_tsap-src_** (I try it on network with 1x 0BA7 and 1x 0BA8 and both of them works).